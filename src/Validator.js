const consign = require('consign');
const Ajv = require('ajv');
const AjvErrors = require('ajv-errors');
const addFormats = require('ajv-formats');

const ajv = new Ajv({allErrors: true});
AjvErrors(ajv);
addFormats(ajv);

const processJsonSchemas = () => {
  const validatorsJsonPath = Object.keys(this.validators);

  for (let i = validatorsJsonPath.length - 1; i >= 0; i -= 1) {
    const validatorJsonPath = validatorsJsonPath[i];
    const httpMethodsOfPath = this.validators[validatorJsonPath];

    for (let numOfHttpMethodsOfPath = httpMethodsOfPath.length - 1;
      numOfHttpMethodsOfPath >= 0; numOfHttpMethodsOfPath -= 1) {
      const schemaForValidate = httpMethodsOfPath[numOfHttpMethodsOfPath];
      this.compileValidator({
        validatorJsonPath,
        schemaPath: schemaForValidate.path,
        method: schemaForValidate.method,
        schema: schemaForValidate.schema,
        options: schemaForValidate.options,
      });
    }
  }
};

const compileValidator = (preCompiledObj) => {
  const {
    validatorJsonPath, schemaPath, method, schema, options,
  } = preCompiledObj;

  const validatorObj = {};
  validatorObj[method] = {};
  validatorObj[method].options = options;
  validatorObj[method].compiledValidator = ajv.compile(schema);

  if (schemaPath === '/' || !schemaPath) {
    this.storeCompiledValidators(`/${validatorJsonPath}`, validatorObj);
  } else {
    this.storeCompiledValidators(`/${validatorJsonPath}/${schemaPath.trim()}`, validatorObj);
  }
};

const storeCompiledValidators = (route, compiledValidator) => {
  if (this.compiledValidators[route]) {
    this.compiledValidators[route] = { ...this.compiledValidators[route], ...compiledValidator };
    return;
  }
  this.compiledValidators[route] = compiledValidator;
};


const validator = (req, res, next) => {
  try {
    const { compiledValidator, options } = this.compiledValidators[req.path][req.method];

    if (req.query?.downloadschema) {
      return res.status(200).json(compiledValidator.schema)
    }

    const optionsToUse = options || this.options;
    compiledValidator(req.body);
    if (!compiledValidator.errors) {
      return next();
    }
    const jsonToReturn = {};
    jsonToReturn[optionsToUse.customKey || 'error'] = `${optionsToUse.customMessage || compiledValidator.errors[0].message}`;
    return res.status(optionsToUse.status || 400).json(jsonToReturn);
  } catch (error) {
    return next();
  }
};

validator.loadJsonSchema = (pathOfJson, options = {}) => {
  this.compiledValidators = {};
  this.processJsonSchemas = processJsonSchemas;
  this.storeCompiledValidators = storeCompiledValidators;
  this.compileValidator = compileValidator;
  this.options = options;


  consign({ cwd: pathOfJson }).include('/validators').into(this);
  if (options.rebuildPath !== undefined) {
    let objectExtracted;
    options.rebuildPath.split('/').forEach((path) => {
      if (objectExtracted == null) {
        objectExtracted = this[path];
      } else {
        objectExtracted = objectExtracted[path];
      }
    });
    this.validators = objectExtracted;
  }
  this.processJsonSchemas();
  return this;
};

module.exports = validator;
