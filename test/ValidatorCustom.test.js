const validator = require('../src/Validator');
const res = require('./Validator.mock');

validator.loadJsonSchema('test', { customMessage: 'Invalid entries. Try again.', customKey: 'message' });


test('Should not call the controller after validate because the the validation must fail', () => {
  const falseReq = {
    path: '/user',
    method: 'POST',
    body: {
      password: 'test',
    },
  };
  const falseNextOfExpress = () => {
    done();
  };

  const result = validator(falseReq, res, falseNextOfExpress);
  expect(result.status).toBe(400);
  expect(result.body.message).toBe('Invalid entries. Try again.');
});

test('Should not call the controller after custom validate because the the validation must fail', () => {
  const falseReq = {
    path: '/userCustom',
    method: 'POST',
    body: {
      password: 'test',
    },
  };
  const falseNextOfExpress = () => {
    done();
  };

  const result = validator(falseReq, res, falseNextOfExpress);
  expect(result.status).toBe(401);
  expect(result.body.message).toBe('Incorrect username or password');
});
