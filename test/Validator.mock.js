this.json = (receivedObj) => {
  const result = {
    status: this.receivedStatus,
    body: receivedObj,
  };
  return result;
};

this.status = (receivedStatus) => {
  this.receivedStatus = receivedStatus;
  return this;
};

module.exports = this;
