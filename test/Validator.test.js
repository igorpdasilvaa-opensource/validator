const validator = require('../src/Validator');
const res = require('./Validator.mock');

const loadedSchemas = validator.loadJsonSchema('test');

test('Should process all validators of the folder validators and store into compiledValidators', () => {
  expect(loadedSchemas.compiledValidators).toHaveProperty('/user');
  expect(loadedSchemas.compiledValidators).not.toHaveProperty('/user/');
  expect(loadedSchemas.compiledValidators).not.toHaveProperty('/user//');
  expect(loadedSchemas.compiledValidators).toHaveProperty('/game/start');
  expect(loadedSchemas.compiledValidators).toHaveProperty('/game/checkpoint');
});

test('Should call the controller without validate because does not exist validator for /user/find with GET method ', (done) => {
  const falseReq = {
    path: '/user/find',
    method: 'GET',
  };
  const falseNextOfExpress = () => {
    done();
  };

  validator(falseReq, null, falseNextOfExpress);
});

test('Should call the controller without validate because does not exist validator for /user on GET method ', (done) => {
  const falseReq = {
    path: '/user',
    method: 'GET',
  };
  const falseNextOfExpress = () => {
    done();
  };

  validator(falseReq, null, falseNextOfExpress);
});

test('Should call the controller only after validate the user route and req.body and the validation must be successful', (done) => {
  const falseReq = {
    path: '/user',
    method: 'POST',
    body: {
      username: 'test',
      password: 'test',
    },
  };
  const falseNextOfExpress = () => {
    done();
  };

  validator(falseReq, null, falseNextOfExpress);
});

test('Should not call the controller after validate because the the validation must fail', () => {
  const falseReq = {
    path: '/user',
    method: 'POST',
    body: {
      password: 'test',
    },
  };
  const falseNextOfExpress = () => {
    throw new Error('This line should not be executed');
  };

  const result = validator(falseReq, res, falseNextOfExpress);
  expect(result.status).toBe(400);
  expect(result.body.error).toBe('must have required property \'username\'');
});
test('Should return the json schema instead of validate', () => {
  const falseReq = {
    path: '/user',
    method: 'POST',
    query: {
      downloadschema: 'true',
    },
    body: {
      password: 'test',
    },
  };
  const falseNextOfExpress = () => {
    throw new Error('This line should not be executed');
  };

  const result = validator(falseReq, res, falseNextOfExpress);
  expect(result.status).toBe(200);
  expect(result.body).toHaveProperty('$schema');
  expect(result.body).toHaveProperty('title');
  expect(result.body).toHaveProperty('description');
  expect(result.body).toHaveProperty('required');
  expect(result.body).toHaveProperty('type');
  expect(result.body).toHaveProperty('properties');
});
