# What is this ? 
Hi, i use AJV to make form validations and i was made to make this more simplest, so just put some jsons in the right place and ou will have a good form validator for you !
# How install
Just copy and paste the code below
```
npm i -S @igorpdasilvaa/validator
```
# How use
This validator is maked to use with express framework, you will need to have a folder called `validator` and put a JSON with the validator. The name o json file is used to know what is the route who will be validated, so if you put a `users.json` anything you put in the json `will have /users` as base route

In addition to this you will need to configure me as a `middleware` in the express, so i suggest to make something like that 

```
const validator = require('@igorpdasilvaa/validator');

validator.loadJsonSchema('src'); // as param pass the parent folder of the validator subfolder

app.use(validator);
```

in addition to this you can find a real usage example in that [link](https://gitlab.com/igorpdasilvaa-opensource/nodejs-skeleton/-/blob/master/src/config/middlewares.js#L14)

# Options to make you form validator more custom
you can pass a custom options object in the json schema as a local option or when you call the loadJsonSchema as a global configuration. In the options object you can pass the following options:

-  customMessage `type string` replace the error message of ajv for a custom message who you want to return to you user when fail in the validation
- customKey `type string` replace the key used to pass the message of error, the default it's `error`, but with that you can change to other this, eg: `message`
- status `type int` replace the default status returned to the client when a error is found, default it's 400

example of usage of options above
```
validator.loadJsonSchema('src',{
    customMessage: 'Wrong information, please try again',
    customKey: 'message',
    status: '401'
});
```